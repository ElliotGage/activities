﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


namespace Activities
{
    class ActivityManager
    {
        private static List<Activity> _activities;
        public static List<Activity> GetActivitiesForTime(string email, DateTime dateTime)
        {
            InitializeData();
            var results = from a in _activities
                          where a.Email == email
                          where a.ActivityDate.Date == dateTime.Date
                          select a;

            return results.ToList<Activity>();
        }

        // query to -+ .05
        public static List<Activity> GetActivitiesForLocation(string email, double latitude, double longitude)
        {
            InitializeData();
            //var results = from a in _activities
            //    where a.Email == email
            //    where a.Latitude <= (latitude +.05) 
            //    where a.Latitude >= (latitude -.05)
            //    where a.Longitude <= (longitude + .05) 
            //    where a.Longitude >= (longitude - .05)
            //    select a;

            var results = from a in _activities
                          where a.Email == email
                          && a.Latitude <= (latitude + .05)
                          && a.Latitude >= (latitude - .05)
                          && a.Longitude <= (longitude + .05)
                          && a.Longitude >= (longitude - .05)
                          select a;

            return results.ToList<Activity>();
        }

        public static void InitializeData()
        {
            _activities = new List<Activity>();
            _activities.Add(new Activity
            {
                Email = "elliot@elliotgage.com",
                ActivityDate = DateTime.Now.Date,
                Latitude = 37.652971,
                Longitude = -120.985638,
                Caption = "Running Modesto",
                ImagePath = "sampleImage.jpg",
                ID = 1
            });
            _activities.Add(new Activity
            {
                Email = "elliot@elliotgage.com",
                ActivityDate = DateTime.Now.Date,
                Latitude = 37.652971,
                Longitude = -120.985638,
                Caption = "Walking Modesto",
                ImagePath = "sampleImage.jpg",
                ID = 2
            });
            _activities.Add(new Activity
            {
                Email = "elliot@elliotgage.com",
                ActivityDate = DateTime.Now.Date.AddDays(1),
                Latitude = 37.652971,
                Longitude = -120.985638,
                Caption = "Walking Modesto",
                ImagePath = "sampleImage.jpg",
                ID = 3
            });
            _activities.Add(new Activity
            {
                Email = "someoneelse@gmail.com",
                ActivityDate = DateTime.Now.Date.AddDays(0),
                Latitude = 37.774929,
                Longitude = -122.419416,
                Caption = "Walking San Francisco",
                ImagePath = "sampleImage.jpg",
                ID = 4
            });
            _activities.Add(new Activity
            {
                Email = "someoneelse@gmail.com",
                ActivityDate = DateTime.Now.Date.AddDays(1),
                Latitude = 37.774929,
                Longitude = -122.419416,
                Caption = "Walking San Francisco",
                ImagePath = "sampleImage.jpg",
                ID = 5
            });
        }
    }
}
