﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Linq;

namespace Activities
{
    class Activity
    {
        private const string URL = "http://prototype.elliotgage.com/uploads/";
        private const string EMPTYIMAGEPATH = "noimageselected.png";
        private string _imagePath;

        public string Caption { get; set; }

        public int ID { get; set; }

        public DateTime ActivityDate { get; set; }

        public string Email { get; set; }

        public double Longitude { get; set; }
        public double Latitude { get; set; }

        public string ImagePath
        {
            get
            {
                return _imagePath;
            }
            set
            {
                if (value.Trim().Length < 1)
                {
                    _imagePath = EMPTYIMAGEPATH;
                }
                else
                {
                    _imagePath = URL + value;
                }

            }
        }
    }
}
