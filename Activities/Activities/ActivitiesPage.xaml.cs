﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Activities
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActivitiesPage : ContentPage
    {
        ObservableCollection<Activity> observableActivityList;

        public ActivitiesPage()
        {
            InitializeComponent();
            List<Activity> activities = ActivityManager.GetActivitiesForTime("elliot@elliotgage.com", DateTime.Now.Date);
            observableActivityList = new ObservableCollection<Activity>(activities);

            SimpleListView.ItemsSource = observableActivityList;
        }

        public async void DoQuery(object sender, EventArgs e)
        {
            double longitude = Convert.ToDouble(this.FindByName<Entry>("Longitude").Text);
            double latitude = Convert.ToDouble(this.FindByName<Entry>("Latitude").Text);
            string email = this.FindByName<Entry>("Email").Text;
            List<Activity> activities = ActivityManager.GetActivitiesForLocation(email, latitude, longitude);
            observableActivityList = new ObservableCollection<Activity>(activities);

            SimpleListView.ItemsSource = observableActivityList;
        }
    }
}